## Documentation

This is a repository that builds simple nodejs hello-world running with docker swarm

It is used to give the documentation which contains detailed instructions to reproduce.

It basically consists of 3 stages:
1. Create the Docker Swarm
2. Build stage where a Docker Image and the simple hello-world app is built
4. Deploy stage where a previously built Docker Image is run on Docker swarm cluster.

### 1. Create the Docker Swarm

1. Create the docker machines  
`  $ docker-machine create --driver virtualbox manager1`  
  `$ docker-machine create --driver virtualbox worker1`  

2. ssh to docker-machine and initialize docker swarm  

   docker-machine manager:  
   `$ docker-machine ssh manager1`  
   `$ docker swarm init --advertise-addr 192.168.99.100`  

   docker-machine worker:  
   `$ docker swarm join --token {generated-token} 192.168.99.100:2377`  


### 2. Build the app and docker image  

1. Create the sample hello-world app like this [index.js](https://gitlab.com/nazmi.adline/nodejs-dockerswarm/blob/master/index.js)  
2. Create the Dockerfile like this [Dockerfile](https://gitlab.com/nazmi.adline/nodejs-dockerswarm/blob/master/Dockerfile)  
3. Build the Dockerfile `$ docker build -t hello .`  

### 3. Deploy 

1. Test the app with compose  
   `$ docker-compose up -d`  
   check if the app is running with  
   `$ curl localhost:8081`  

2. Deploy to the swarm  
   `$ docker stack deploy --compose-file docker-compose.yml helloworld`  
   Check that it’s running with  
   `$ docker stack services helloworld`  

   Once it’s running, you should see 1/1 under REPLICAS for both services.  
   This might take some time if you have a multi-node swarm, as images need to be pulled.  

3. Test the app with `curl http://address-of-other-node:port`  

    `curl http://192.168.99.100:8081`  
    `curl http://192.168.99.101:8081`  


#### Here is the evidence of my test result  
[SCREENSHOT LINK](https://drive.google.com/drive/folders/17vHgn2Y-Lk84Um_B0HFqPS5BTdltafgX?usp=sharing)  
#### Click the link above   



                                                       Adline Nazmi©

